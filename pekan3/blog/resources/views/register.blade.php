<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
        <label>First Name : </label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name : </label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Tanggal Lahir</label><br><br>
        <input type="date" name="ttl"><br><br>
        <label>Gender : </label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality</label><br><br>
        <select name="nationality"><br><br>
            <option value="Indonesian">Indonesian</option>
            <option value="Austrian">Austrian</option>
            <option value="British">British</option>
            <option value="Russian">Russian</option>
            <option value="American">American</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio: </label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit">
    </form>

</body>
</html>