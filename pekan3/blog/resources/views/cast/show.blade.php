@extends('adminlte.master')

@section('header')
    Biodata {{$cast->nama}}
@endsection

@section('content')
    <h2>Show Cast {{$cast->id}}</h2>
    <h4>{{$cast->nama}}</h4>
    <p>Umur : {{$cast->umur}}</p>
    <p>Bio  : {{$cast->bio}}</p>
@endsection