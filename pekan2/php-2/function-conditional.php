<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
//Soal 1

function greetings($nama) {
    echo "Halo $nama, Selamat Datang di Sanbercode! <br>";
  }

greetings("Bagas");
greetings("Wahyu");
greetings("Abdul");

echo "<br>";



//Soal 2
echo "<h3>Soal No 2 Reverse String</h3>";
function reverse($kata1){
    $count = strlen($kata1);
    $result = "";
    for ($x=$count-1; $x >=0 ; $x--){
        $result = $result.$kata1[$x];
    }
    return $result;
}

function reverseString($kata2){
    $balik = reverse($kata2);
    echo $balik."<br>";
}

reverseString("abduh");
reverseString("Sanbercode");
reverseString("We Are Sanbers Developers");
echo "<br>";


//Soal 3
echo "<h3>Soal No 3 Palindrome </h3>";
function palindrome($kata3){
    $balik = reverse($kata3);
    if ($kata3 === $balik){
        echo "$kata3 => True <br>";
    }else{
        echo "$kata3 => False <br>";
    }
}
palindrome("civic") ;
palindrome("nababan") ;
palindrome("jambaban");
palindrome("racecar");


//Soal 4
echo "<h3>Soal No 4 Tentukan Nilai </h3>";
function tentukan_nilai($number){
    $output = "";
    if ($number>=85 && $number<=100){
        $output = "$number Sangat Baik <br>";
    }else if($number>70 && $number<85){
        $output = "$number Baik <br>";
    }else if($number>60 && $number<70){
        $output = "$number Cukup <br>";
    }else{
        $output = "$number Kurang <br>";
    }
    return $output;
}
echo tentukan_nilai(98);
echo tentukan_nilai(76);
echo tentukan_nilai(67);
echo tentukan_nilai(43);


?>

</body>

</html>