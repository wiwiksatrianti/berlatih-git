<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        //Soal 1
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        echo "LOOPING PERTAMA<BR>";
        $i = 2;

        while($i <= 20) {
            echo "$i - I Love PHP<br>";
        $i+=2;
        }

        echo "<BR>LOOPING KEDUA<BR>";
        $j = 20;
        while($j >= 2) {
            echo "$j - I Love PHP<br>";
        $j-=2;
        }
        echo "<h3>Soal No 2 Looping Array Modulo </h3>";


        //Soal 2
        $numbers = [18, 45, 29, 61, 47, 34]; 
        echo "array numbers: ";
        print_r($numbers);
        foreach ($numbers as $value) {
            $rest[] = $value+5;
          }
        
        echo "<br>";
        echo "Array tambah 5 adalah:  "; 
        print_r($rest);
        echo "<br>";

        //Soal 3
        echo "<h3> Soal No 3 Looping Asociative Array </h3>";

        $items = [
            ["id" =>'001', "name" =>'Keyboard Logitek', "price" =>60000, "description" =>'Keyboard yang mantap untuk kantoran', "source" =>'logitek.jpeg'], 
            ["id" =>'002', "name" =>'Keyboard MSI', "price" =>300000, "description" =>'Keyboard gaming MSI mekanik', "source" =>'msi.jpeg'],
            ["id" =>'003', "name" =>'Mouse Genius', "price" =>50000, "description" =>'Mouse Genius biar lebih pinter', "source" =>'genius.jpeg'],
            ["id" =>'004', "name" =>'Mouse Jerry', "price" =>30000, "description" => 'Mouse yang disukai kucing', "source" =>'jerry.jpeg']
        ];
        foreach($items as $item) {
            print_r ($item);
            echo "<br>";
        }
        
        echo "<h3>Soal No 4 Asterix </h3>";
        

        //Soal 4
        echo "Asterix: ";
        echo "<br>";
        
        for ($x=1; $x<=5; $x++){
            for ($y=1; $y<=$x; $y++){
                echo "* ";
            }
            echo "<br>";
        }
    ?>

</body>
</html>
